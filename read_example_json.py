import json
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    with open("example.json", "r") as f:
        target_data = json.load(f)

    print(target_data)

    print(target_data['start_condition']['temperature'])
    start_temp = target_data['start_condition']['temperature']
    end_temp = target_data['end_condition']['temperature']
    interval = target_data['interval']['temperature']

    # startからendまで、np.linspaceでinterval間隔で補完する
    num = int((end_temp - start_temp) / interval) + 1
    list_temp = np.linspace(start_temp, end_temp, num)

    # plot temperature graph
    plt.plot(list_temp, label="temperature", marker="o", linestyle=" ")
    plt.show()
